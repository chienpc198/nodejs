require('../src/db/mongoose')
const User = require('../src/models/user')

/* User.findByIdAndUpdate('62c23ab5552a5cd921b61f71', { age: 1 }).then((user) => {
    console.log(user)
    return  User.countDocuments({ age: 1 })
}).then((result) => {
    console.log(result)
}).catch((e) => {
    console.log(e)
}) */


const findAndUpdateUser = async (id, age) => {
    const user = User.findByIdAndUpdate(id, { age: age})
    const count = User.countDocuments({age: age})

    return count
}

findAndUpdateUser('62c23ab5552a5cd921b61f71', 0).then((count) => {
    console.log(count)
}).catch((e) => {
    console.log(e)
})

