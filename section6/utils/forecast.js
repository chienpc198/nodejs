const request = require('request')

const forecast = (location, callback) => {
    const url = 'http://api.weatherstack.com/current?access_key=032f13ac8e1a8618adadda87f0da0976&query=' + encodeURIComponent(location)
    
    request(url, (error, response) => {
        if(error){
            callback('unable to connect to service')
        } else if(response.body.error){
            callback('unable to find location')
        } else{
            callback(response.body.current.wind_speed)
        }
    })
}

module.export = forecast