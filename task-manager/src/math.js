const calculateTip = (total, tipPersent = .25) => {
    setTimeout(() => {
        const tip = total * tipPersent
        return total + tip 
    }, 2000)
    
}

module.exports = {
    calculateTip
}