require('../src/db/mongoose')
const Task = require('../src/models/task')

/* Task.findByIdAndRemove('62c2425c6b84d943b2854dc4', {}).then((task) => {
    console.log(task)
    return Task.countDocuments({ conpleted: false })
}).then((result) => {
    console.log(result)
}).catch((error) => {
    console.log(error)
}) */


const findAndRemoveTask = async (id) => {
    const task = await Task.findByIdAndRemove(id, {})
    const count = await Task.countDocuments({ completed: false })

    return count
}

findAndRemoveTask('62c296726ab6c0964b98ba79').then((count) => {
    console.log(count)
}).catch((e) => {
    console.log(e)
})