const testPromise = new Promise((resolve, reject) => {
    setTimeout(() => {
        if (true) resolve([1, 2, 3])
        else reject('oop')
    }, 2000)
})

testPromise.then((result) => {
    console.log('success ' + result)
}).catch((error) => {
    console.log('fail ' + error)
})