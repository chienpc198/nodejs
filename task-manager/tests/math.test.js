const { calculateTip } = require('../src/math')

test('Should calculate total with tip', () => {
    const total = calculateTip(10, .3)
    expect(total).toBe(13)
})

test('Async test demo', async () => {
    const total = await calculateTip(10, .3)
    expect(total).toBe(13)
    
})