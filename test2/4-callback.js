/* setTimeout(() => {
    console.log('two seconds are up')
}, 2000)

const geocode = (address, callback) => {
    setTimeout(() => {
        const data = {
            latitude: 0, 
            longtitude: 0
        }


        callback(data)
    }, 2000);
}

geocode('hanoi', (data) => {
    console.log(data)
})

const sum = (num1, num2, callback) =>{
    setTimeout(() => {
        const sum = num1 + num2

        callback(sum)
    }, 2000);
}

sum(1, 4, (sum) =>{
    console.log(sum)
}) */


const testCallback = (callback) => {
    setTimeout(() => {
        callback(undefined, [1, 4, 7])
    }, 2000);
}

testCallback((error, response) => {
    if (error) {
        return console.log('error')
    }

    console.log(response)
})