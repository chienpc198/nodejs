const request = require('supertest')
const jwt = require('jsonwebtoken')
const mongoose = require('mongoose')
const app = require('../src/app')
const User = require('../src/models/user')


const userOneId = new mongoose.Types.ObjectId()
const userOne = {
    _id: userOneId,
    name: 'huyen',
    gmail: 'chienpc198@gmail.com',
    password: 'chien12345',
    tokens: [{
        token: jwt.sign({ _id: userOneId }, process.env.JWT_SECRET)
    }]
}

beforeEach(async () => {
    await User.deleteMany()
    await new User(userOne).save()
})

test('Should signup a new user', async () => {
    const reponse = await request(app).post('/users').send({
        name: 'chien',
        email: 'phamhuyen17456@gmail.com',
        password: 'chien12345'
    }).expect(201)

    const user = await User.findById(reponse.body.user._id)
    expect(user).not.toBeNull()
})

test('Should get profile for user', async () => {
    const setText = 'Bearer ' + userOne.tokens[0].token 
    const getText = '/users'
    await request(app)
        .get(getText)
        .set('Authorization', setText)
        .send()
        .expect(200)
})

test('Should delete account for user', async () => {
    const setText = 'Bearer ' + userOne.tokens[0].token  
    await request(app)
        .delete('/users/me')
        .set('Authorization', setText)
        .send()
        .expect(200)
})

test('Should upload avatar image', async () => {
    const setText = 'Bearer ' + userOne.tokens[0].token 
    await request(app)
        .post('/users/me/avatar')
        .set('Authorization', setText)
        .attach('avatar', 'tests/fixtures/avatar2.jpg')
        .expect(200)
    const user = await User.findById(userOneId)
    expect(user.avatar).toEqual(expect.any(Buffer))
})