var notes = require('./notes.js');
var validator = require('validator');
var yargs = require('yargs');


// var sum = runAdd(2, 3);

// console.log(sum);

// console.log(validator.isEmail('a@mail.com'));

yargs.command({
    command: 'add',
    describe: 'add a new note',
    builder: {
        title: {
            describe: 'Note title',
            demandOption: true,
            type: 'string'
        },
        body: {
            describe: 'Note body',
            demandOption: true,
            type: 'string'
        }
    },
    handler: function(argv) {
        notes.addNote(argv.title, argv.body)
    }
})


yargs.command({
    command: 'read',
    describe: 'read note',
    handler: function(){
        console.log('reading note')
    }
})

yargs.command({
    command: 'remove',
    describe: 'remove note',
    builder: {
        title: {
            describe: 'note title',
            demandOption: true,
            type: 'string'
        }
    },
    handler: function(argv){
        console.log('1')
        notes.removeNote(argv.title)
        console.log('2')
    }
})

yargs.parse()