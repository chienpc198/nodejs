const weatherForm = document.querySelector('.search');
const search = document.querySelector('input');
const message1 = document.querySelector('#message-1')
const message2 = document.querySelector('#message-2')

weatherForm.addEventListener('submit', (e) => {

    e.preventDefault();

    const location = search.value;

    const url = 'http://localhost:3000/weather?address=' + encodeURIComponent(location);
    
    fetch(url).then((response) => {
        response.json().then((data) => {
            if(data.error){
                message1.textContent = data.error;
            } else {
                message1.textContent = 'temparature: ' + data.forecast_temperature;
                message2.textContent = 'address: ' + data.address;
            }
        })
    })


    console.log(location);
});