const express = require('express')
const router = new express.Router()
const Task = require('../models/task')
const auth = require('../middleware/auth')

router.post('/tasks', auth, async (req, res) => {
    const task = new Task({
        ...req.body,
        owner: req.user._id
    })

    try {
        await task.save()
        res.send(task)
    } catch(e) {
        res.status(400).send(e)
    }

    /* task.save().then(() => {
        res.send(task)
    }).catch((error) => {
        res.status(400).send(error)
    }) */
})

router.get('/tasks/:id', auth, async (req, res) => {
    const _id = req.params.id
    try {
        const task = await Task.findOne({_id, owner: req.user._id})
        if(!task) {
            return res.status(404).send()
        }
        res.send(task)
    } catch(e) {
        res.status(500).send(e)
    }

    /* Task.find({}).then((tasks) => {
        res.send(tasks)
    }).catch((error) => {
        res.status(500).send(error)
    }) */
})

router.get('/tasks', auth, async (req, res) => {

    const match = {}
        if ( req.query.completed) {
            match.completed = req.query.completed === true
        }

    try {

        const match = {}

        if (req.query.completed) {
            match.completed = req.query.completed
        }

        match.owner = req.user._id
        match.options = {
            limit: parseInt(req.query.limit)
        }

        const tasks = await Task.find(match) 
        res.send(tasks)

        /* await req.user.populate({
            path:'tasks',
            match,
            options: {
                limit: parseInt(req.query.limit)
            }
        }).execPopulate()
        res.send(req.user.tasks) */
        
    } catch(e) {
        res.status(500).send(e)
    }
})

router.patch('/tasks/:id', auth, async (req, res) => {

    const updates = Object.keys(req.body)
    const allowedUpdate = ['description', 'completed']
    const isValidOperation = updates.every((update) => allowedUpdate.includes(update))

    if(!isValidOperation){
        res.status(404).send()
    }

    try {
        //const task = await Task.findByIdAndUpdate(req.params.id, req.body, {new: true, runValidators: true})
        const task = await Task.findOne({_id: req.params.id, owner: req.user._id})

        if(!task) {
            res.status(404).send()
        }

        //const task = await Task.findById(req.params.id)
        updates.forEach((update) => task[update] = req.body[update])
        await task.save()

        res.send(task)

    } catch(e) {
        res.status(400).send(e)
    }
})

router.delete('/tasks/:id', auth, async (req, res) => {
    try {
        const task = await Task.findByIdAndDelete({_id: req.params.id, owner: req.user._id})
        if(!task) {
            res.status(404).send()
        }
        res.send(task)
    } catch(e) {
        res.status(500).send()
    }
})

module.exports = router