const express = require('express')
const path = require('path')
const hbs = require('hbs')
const forecast = require('./utils/forecast')
const geocode = require('./utils/geocode')

console.log(path.join(__dirname, '../public'))





const app = express()

// defind path for express config
const publicDirectoryPath = path.join(__dirname, '../public')
const viewsPath = path.join(__dirname, '../templates/views')
const particalsPath = path.join(__dirname, '../templates/particals')

app.set('view engine', 'hbs')
app.set('views', viewsPath)
app.use(express.static(publicDirectoryPath))
hbs.registerPartials(particalsPath)


app.get('', (req, res) => {
    res.render('index', {
        title: 'weather',
        name: 'person0'
    })
})

app.get('/help', (req, res) => {
    res.render('help', {
        title: 'helpfull thing',
        text: 'this is help',
        name: 'person1'
    })
})

app.get('/about', (req, res) => {
    res.render('about', {
        title: 'about somthing',
        text: 'about me',
        name: 'person2'
    })
})

app.get('/weather', (req, res) => {
    if (!req.query.address) {
        return res.send({
            error: 'You must provide an address!'
        })
    }


        forecast(req.query.address, (error, forecastData) => {
            if (error) {
                return res.send({ error })
            }

            res.send({
                forecast_temperature: forecastData,
                address: req.query.address
            })
        })
})

app.get('/products', (req, res) => {
    if(!req.query.search){
        return res.send({
            error: 'must provide search'
        })
    }

    res.send({
        products: []
    })
})

app.get('/help/*', (req, res) => {
    res.render('404', {
        title: '404: not found',
        text: 'help acticle not found'
    })
})

app.get('*', (req, res) => {
    res.render('404', {
        title: '404: sdfasdf',
        text: 'found nothing'
    })
})

app.listen(3000, () => {
    console.log("server is up on port 3000")
})