const fs = require('fs')


const getNotes = function(){

}

const addNote = function(title, body){
    const notes = loadNote()
    notes.push({
        title: title,
        body: body
    })

    saveNote(notes)
}

const removeNote = function(title){
    const notes = loadNote()
    const notesToKeep = notes.filter(function (note){
        return note.title !== title
    })
    saveNote(notesToKeep)
}

const saveNote = function(notes){
    const notesJSON = JSON.stringify(notes)
    fs.writeFileSync('notes.json', notesJSON)
}

const loadNote = function(){
    try{
        const dataBuffers = fs.readFileSync('notes.json');
        const data = dataBuffers.toString()
        return JSON.parse(data)
    }
    catch{
        return []
    }
}



module.exports = {
    getNotes: getNotes,
    addNote: addNote,
    removeNote: removeNote
}