const tasks = {
    tasks: [{
        text: 'shopping',
        completed: true
    }, {
        text: 'clear yard',
        completed: false
    }, {
        text: 'film course',
        completed: false
    }],

    getTasksToDo() {
        return this.tasks.filter((task) =>  task.completed === false)
    }
}

console.log(tasks.getTasksToDo())